
//: Playground - noun: a place where people can play

//iOS Programozás, választható tantárgy 2020
//1. Labor Playground és bevezetés a Swift nyelvbe

import Foundation
import UIKit

// VARIABLES

// 1. (0.5 p) Declare a constant array favouriteNumbers with values for your favourite numbers.


let favouriteNumbers:[Int] = [3, 7, 13, 20]
print(favouriteNumbers.count)


// 2. (0.5 p) Declare a dictionary with the name of your colleagues and the "department" he/she is working on.

let colleagues: [String: String] = ["Anett":"Informatics",
                                    "Krisztian":"Informatics",
                                    "Anita":"Mathematics",
                                    "Peter":"Physics"]

// 3. (0.5 p) Declare an array of tuples with the list of your colleagues and their height.
// ex. tuple: ("Big Guy", 201)

var heights:[(name: String, height: Double)] = [("Anita", 170),
                                                ("Krisztian", 178),
                                                ("Anett", 170),
                                                ("Peter", 182)]

// 4. (0.5 p) Order the array created above in ascending order (based on height)
// use `sort`

heights.sort { h1, h2 in
    return h1.height < h2.height
}
// 5. (0.5 p) Group the dictionary by department.
// use the: `Dictionary(grouping: ..., by: ...)` initializer

let groupByCategory = Dictionary(grouping: colleagues, by: { $0.1})

// 6. (0.5 p) Order the tuple based on the heights, if there are entries with the same height, order them by alphabetical order.
// hint: the `sort` function can take two propeties as parameter

heights.sort { h1, h2 in
    return (h1.height, h1.name) < (h2.height, h2.name)
}

// 7. (0.5 p) Increment every number by one in favouriteNumbers.
// use `map`

let incrementedFavNumbers = favouriteNumbers.map {$0 + 1}

// 8. (0.5 p) Print out only the name of your colleagues from the dictionary.

colleagues.map {(name, height) in print(name)}

// 9. (1 p) Print out the average height of your colleagues.
// use `map` + `reduce`


let values = heights.map{$0.1}
let average = values.reduce(0.0) { $0 + $1 } / Double(values.count)
print(average)

// 10. (1 p) Split the array in half.
// hint: you can access a subarray like this: `someArray[..<n]` or `someArray[n...]`
let count = favouriteNumbers.count
let half = count / 2
let leftSplit = favouriteNumbers[0 ..< half]
let rightSplit = favouriteNumbers[half ..< count]

// 11. (1 p) Declare the first 10 Fibonacci numbers in a set. (1,1,2,3,...)
// Obviously the number 1 will appear only once in this set (properties of a set)
// write the algorithm, (implement a function like this: `func fibonacci(n: Int) -> Set<Int>`)

func fibonacci(n: Int) -> Set<Int> {
    return Set((1...n-1).reduce([1, 1]) { (f, _) in f + [f.last! + f[f.count - 2]] })
}

let fib = fibonacci(n:10)
print(fib)

// 12. (1 p) Get the intersect of two sets: the first 10 Fibonacci numbers and the first 10 prime numbers.
// there is a method `intersection`

let primes: Array = [Int](2...100).filter({
                        num in [Int](1...Int(sqrt(Float(num)))).filter({
                            num % $0 == 0
                        }).count == 1
                    })
let setOfPrimes:Set<Int> = Set(primes[0...9])
print(setOfPrimes)

let intersectionOfSets = setOfPrimes.intersection(fib)
print(intersectionOfSets)

// CONTROL FLOW

// 13. (1 p) Take a number and print the reversed version of it.
// write the algorithm (implement a function like this: `func reverse(number: Int) -> Int`)
func reverse(number: Int) -> Int {
    var n: Int = number
    var reverse: Int = 0
    while (n != 0) {
        reverse = reverse * 10
        reverse = reverse + n % 10
        n = n / 10
    }
    return reverse
}
let reverseVersion = reverse(number: 354)
print(reverseVersion)

// 14. (1 p) Choose a number and find out if it's palindrome.
// use the solution from above

func palindrom(number: Int) -> Bool {
    return reverse(number: number) == number
}

if(palindrom(number: 122)) {
    print("palindrom")
} else {
    print("not palindrome")
}

// CLASSES AND STRUCTS

// 15. (1 p) Write a struct named Course which will have a String property name and an Int one mark.
// ex. struct Sth { aProperty: Type, ..}

struct Course {
    var name: String
    var mark: Int
}

// 16. (1 p) Create a class named Student which will have a private String property name and a private array of courses which he attends to. Both properties will be initialized in the constructor.

class Student {
    private let name: String
    private let courses: Array<Course>
    private let myEnum: myEnum
    
    
    init(name: String, courses: Array<Course>, myEnum: myEnum = .normal) {
        self.name = name
        self.courses = courses
        self.myEnum = myEnum
    }
}

// ENUMS

// 17. (1 p) Create an enum type which will have two cases: normal and scholar.
// ex. enum Sth { case fist, case last }

enum myEnum{
    case normal
    case scholar
    var output: String {
        switch self{
            case .normal:
            return "This is a normal Student"
            case .scholar:
            return "This is a scholar student"
            
        }
    }
}

// 18.( 1 p) Create a String property in the enum (e.g. description) which will return a textual representation of each case (e.g. "This is a scholar student.").
// use the `switch` statement for setting the right textual representation


// 19. (1 p) Add this enum as a property to the above mentioned Student class.


// 20. (1 p) Modify the Student class constructor so the type type parameter has a default value of .normal).
// HINT: you can give a default value in the constructor like `init(..., type: Type = .aType)`



// PROTOCOLS

// 21. (1 p) Vehicles can have different properties and functionality.
// All Vehicles:
// • Have a speed at which they move
// • Calculate the duration it will take them to travel a certain distance
// All Vehicles except a Motorcycle
// • Have an amount of Windows
// Only Buses:
// • Have a seating capacity
// Create the following Vehicles types: Car, Bus, Motorcycle. Do not use subclassing, use protocols instead. Create an array over the minimum required protocol and put an instance of every type in it and print the the following text for every item of the array (if it's possible): " *type* has *amount of windows* windows and needs *time* to travel 100 kilometers. "

protocol Vehicle {
    var speed: Double { get set }
    mutating func calculateDuration(distance: Double) -> Double
}

protocol WindowVehicle: Vehicle {
    // TODO: add the needed property(s)
    var amountOfWindows: Int { get set }
    
}

struct Bus: WindowVehicle {
    var speed: Double
    func calculateDuration(distance: Double) -> Double {
        return distance * 60 / speed
    }
    
    // TODO: implement what's needed and add the needed property(s)
    var amountOfWindows: Int
    var seatingCapaacity: Int
}

// TODO: implement the other types
struct Motocycle: Vehicle {
    var speed: Double
    
    func calculateDuration(distance: Double) -> Double {
        return distance * 80 / speed
    }
}


// TODO: add elements to this array
var vehicles: [Vehicle] =
    [Bus(speed: 60, amountOfWindows: 10, seatingCapaacity: 30),
     Motocycle(speed: 100)
     ]


// EXTENSTIONS

// 22. (1 p) Create Int extensions for:
// • Radian value of a degree (computed property)
// • Array of digits of Int (computed property)
extension Int {
    
    func toRadian () -> Double {
        return Double.pi * Double(self) / 180
    }
    
    // TODO: Array of digits
    func arrayOfDigits () -> Array<Int> {
        return  Array(String(self)).compactMap { Int(String($0)) }

    }
}

123.arrayOfDigits()

// 23. (1 p) Create String extensions for:
// • Check if the string contains the character 'a' or 'A'

extension String {
    func contains() -> Bool{
        let charset = CharacterSet(charactersIn: "aA")
        return self.rangeOfCharacter(from: charset) != nil
    }
}

// 24. (1 p) Create Date extensions for:
// • Check if a date is in the future (computed property)
// • Check if a date is in the past (computed property)
// • Check if a date is in today (computed property)

extension Date{
    func isToday() -> Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    // TODO: is in the future
    func future() -> Bool {
        return self > Date()
    }
    
    // TODO: is in the past
    func past() -> Bool {
        return self < Date()
    }
}


let formatter = DateFormatter()
formatter.dateFormat = "yyyy/MM/dd"
let date: Date = formatter.date(from: "2018/03/20")!
let today = Date()
// TODO: test the written extension
let futureDate: Date = formatter.date(from: "2022/03/20")!

print(futureDate.future())
print(date.future())
print(date.past())
print(futureDate.past())


// CLOSURES

// 25. (1 p) Declare a closure that takes an Integer as an argument and returns if the number is odd or even (using regular syntax and shorthand parameter names)
// ex: `let empty: (String) -> Bool = { $0.isEmpty }` - closure for testing if a string is empty


let myClosure: (Int) -> Bool = { $0 % 2 == 1 || $0 % 2  == 0 }
myClosure(5)

//let odd = numbers.filter { $0 % 2 == 1 }
// 26. (1 p) Use the above defined closure to filter out odd numbers from an array of random numbers (use the filter function)
func makeRandomList(n: Int) -> [Int] {
    var result: [Int] = []
    for _ in 0..<n {
        result.append(Int(arc4random_uniform(100) + 1))
    }
    return result
}
// TODO: do the filtering
let list = makeRandomList(n: 8)
let odd = list.filter { $0 % 2 == 1 }
print(odd)

// 27. (1 p) Declare a closure that takes 2 Integers as parameters and returns true if the first argument is larger than the second and false otherwise (using regular syntax and shorthand parameter names)


let larger: (Int, Int) -> Bool = { return $0 > $1}




