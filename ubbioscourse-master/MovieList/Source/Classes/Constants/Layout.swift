//
//  Layout.swift
//  Workeepr
//
//  Created by Hanna Kovacs on 27/10/2020.
//

import struct CoreGraphics.CGFloat

public extension CGFloat {
    /// 2
    static let smallCornerRadius: CGFloat = 2
    /// 4
    static let cornerRadius: CGFloat = 4
    /// 6
    static let mediumCornerRadius: CGFloat = 6
    /// 8
    static let bigCornerRadius: CGFloat = 8
    ///10
    static let biggerCornerRadius: CGFloat = 10
    ///12
    static let bigggestCornerRadius: CGFloat = 12
    
    
    /// 2
    static let smallPadding: CGFloat = 2
    /// 4
    static let halfPadding: CGFloat = 4
    /// 8
    static let padding: CGFloat = 8
    /// 16
    static let padding2x: CGFloat = padding * 2
    /// 24
    static let padding3x: CGFloat = padding * 3
    /// 32
    static let padding4x: CGFloat = padding * 4
    /// 48
    static let padding6x: CGFloat = padding * 6
    /// 56
    static let padding7x: CGFloat = padding * 7
    /// 64
    static let padding8x: CGFloat = padding * 8
    /// 72
    static let padding9x: CGFloat = padding * 6
    /// 132
    static let extraLargePadding: CGFloat = 132.0
    
    /// custom paddings
    static let oneAndHalfPadding: CGFloat = 12.0

    static let titleTopPadding: CGFloat = 11.0
    
    static let favButtonPadding: CGFloat = 14.0

    /// 0.5
    static let mediumOpacity: CGFloat = 0.5
    /// 1
    static let fullOpacity: CGFloat = 1.0
    
    static let mBigPadding: CGFloat = 99.0

    static let imageWidth: CGFloat = 379.0

    static let imageHeight: CGFloat = 213.0

    static let titleLineHeight: CGFloat = 48.0

    static let titleFontSize: CGFloat = 40.0
    
    
    static let thumbnailHeight: CGFloat = 150.0
    
    static let thumbnailWidth: CGFloat = 140.0
    
    static let favButtonHeight: CGFloat = 19.0
    
    static let favbuttonWidth: CGFloat = 20.0

}
