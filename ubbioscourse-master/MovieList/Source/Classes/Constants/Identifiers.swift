//
//  Identifiers.swift
//  MovieList
//
//  Created by babycougar on 16.05.2021.
//

import Foundation

enum Identifiers: String {
    case movieIdentifier  = "movieIdentifier"
    case movieHeaderIdentifier = "movieSectionHeaderIdentifier"
}
