//
//  RequestRepresentable.swift
//  packmen
//
//  Created by Alexandra Muresan on 20/07/2018.
//  Copyright © 2018 Halcyon Mobile. All rights reserved.
//

import Foundation

public typealias Parameters  = [String: Any]

protocol RequestRepresentable {
    var suffix: String { get }
    var method: HTTPMethod { get }
    var parameters: Parameters? { get }
}
