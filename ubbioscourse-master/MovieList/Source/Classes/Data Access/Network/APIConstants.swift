//
//  APIConstants.swift
//  Acceptance Tests
//
//  Created by Halcyon Mobile on 08/01/2019.
//

import Foundation

public enum HTTPMethod: String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case delete  = "DELETE"
}

public typealias HTTPHeaders = [String: String]
public typealias JSONDict = [String: Any]

enum API {
    static let backendURL = "https://foo.bar/api"

    enum Path {
        static let login        = "/auth/login"
        static let signup       = "/auth/signup"
    }

    enum Param {
        static let id           = "id"
        static let email        = "email"
        static let password     = "password"
    }
}
