//
//  UIImage+Color.swift
//  Acceptance Tests
//
//  Created by Halcyon Mobile on 07/02/2019.
//

import UIKit

extension UIImage {

    // https://stackoverflow.com/questions/14523348/how-to-change-the-background-color-of-a-uibutton-while-its-highlighted
    static func with(color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()

        context?.setFillColor(color.cgColor)
        context?.fill(rect)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image
    }
}

extension UIColor {
    static let lightGray: UIColor = UIColor(named: "lightGray")!
    static let lighterBlue: UIColor = UIColor(named: "lighterBlue")!
    static let separatorColor: UIColor = UIColor(named: "separatorColor")!
}
