//
//  AppEngine.swift
//  project-skeleton
//
//  Created by Halcyon Mobile on 9/22/17.
//  Copyright © 2017 Halcyon Mobile. All rights reserved.
//

import Foundation

class AppEngine {

    // MARK: - Lifecycle

    init() {
        // TODO: Setup logging and Fabric
    }

    // MARK: - Application events

    func prepareAppStart() {
        startDebugToolsIfNeeded()
    }
}

extension AppEngine {
    fileprivate func startDebugToolsIfNeeded() {
        // TODO: Start debugging tools
    }
}
