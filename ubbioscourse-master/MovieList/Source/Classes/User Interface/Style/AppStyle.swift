//
//  StyleKit.swift
//  project-skeleton
//
//  Created by Halcyon Mobile on 12/03/2017.
//  Copyright © 2017 Halcyon Mobile. All rights reserved.
//

import UIKit

struct AppStyle {

    static func setupAppearance() {
        // TODO: Set up appearances if needed
//        let switchControlAppearance = UISwitch.appearance()
//        switchControlAppearance.onTintColor = Colors.switchControlOn
    }

    struct Layout {
        static let padding: CGFloat                 = 20.0
    }
}
