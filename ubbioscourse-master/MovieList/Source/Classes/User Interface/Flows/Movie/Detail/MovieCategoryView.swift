//
//  MovieCategoryView.swift
//  MovieList
//
//  Created by babycougar on 21.04.2021.
//

import UIKit

final class MovieCategoryView: UIView {
    
    // MARK: - Public properties
    
    var name: String? {
        didSet {
            categoryLabel.text = name
        }
    }
    
    // MARK: - Private properties
    
    private var categoryLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12.0)
        label.textColor = .black
    
        label.layer.cornerRadius = 4.0
        label.layer.masksToBounds = true
        return label
    }()
    
    // MARK: - UI
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initUI()
        initLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initUI() {
        backgroundColor = .lightGray
        layer.cornerRadius = 4.0
        layer.masksToBounds = true
        addSubview(categoryLabel)
    }
    
    private func initLayout () {
        categoryLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            categoryLabel.topAnchor.constraint(equalTo: topAnchor, constant: .smallPadding),
            categoryLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -.smallPadding),
            categoryLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .padding),
            categoryLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.padding)
        ]
        NSLayoutConstraint.activate(constraints)
 
    }

}
