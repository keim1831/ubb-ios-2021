//
//  LoginViewController.swift
//  Workeepr
//
//  Created by Hanna Kovacs on 27/10/2020.
//

import UIKit

final class MovieDetailViewController: UIViewController {
    
    // MARK: - Private properties
    
    private let movie: Movie
    private var containerView = UIView()
    private var movieImage: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = .bigggestCornerRadius
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private var movieTitle: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: .titleFontSize)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private var blueSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .lighterBlue
        return view
    }()
    
    private var detailLabel = {(text: String) -> UILabel in
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 18.0)
        label.textAlignment = .center
        return label
    }
    
    private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = .padding
        return stackView
    }()
    
    private var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    private var categoryStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = .padding
        return stackView
    }()
    
    private var category = {(name: String) -> MovieCategoryView in
        let view = MovieCategoryView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.name = name
        return view
    }

    private func simpleSeparatorView () -> UIView {
        let view = UIView()
        view.widthAnchor.constraint(equalToConstant: 1).isActive = true
        view.backgroundColor = .black
        return view
    }
    
    private var graySeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .separatorColor
        view.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
        view.alpha = 0.08
        return view
    }()
    
    private var scrollView = UIScrollView()
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(movie: Movie) {
        self.movie = movie
        super.init(nibName: nil, bundle: nil)
    }
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        initMovieParameters()
        initConstraints()
    }

    // MARK: - UI

    private func initUI() {
        view.backgroundColor = .white
        scrollView.addSubview(containerView)

        containerView.addSubview(movieImage)
        containerView.addSubview(movieTitle)
        containerView.addSubview(blueSeparatorView)
        
        stackView.addArrangedSubview(detailLabel(movie.year))
        stackView.addArrangedSubview(simpleSeparatorView())
        stackView.addArrangedSubview(detailLabel(movie.ageGroup))
        stackView.addArrangedSubview(simpleSeparatorView())
        stackView.addArrangedSubview(detailLabel(movie.duration))
        stackView.addArrangedSubview(simpleSeparatorView())
        stackView.addArrangedSubview(detailLabel(movie.language))
        
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(stackView)
        
        for actulaCategory in movie.categories {
            categoryStackView.addArrangedSubview(category(actulaCategory))
        }
        
        containerView.addSubview(categoryStackView)
        containerView.addSubview(graySeparatorView)
        view.addSubview(scrollView)
                
    }

    private func initConstraints() {
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        movieImage.translatesAutoresizingMaskIntoConstraints = false
        movieTitle.translatesAutoresizingMaskIntoConstraints = false
        blueSeparatorView.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        simpleSeparatorView().translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        categoryStackView.translatesAutoresizingMaskIntoConstraints = false
        graySeparatorView.translatesAutoresizingMaskIntoConstraints = false
                
        let constraints = [
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            containerView.widthAnchor.constraint(equalTo: view.widthAnchor),
            
            movieImage.heightAnchor.constraint(equalToConstant: .imageHeight),
            movieImage.topAnchor.constraint(equalTo: containerView.topAnchor, constant: .padding2x),
            movieImage.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: .padding2x),
            movieImage.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -.padding2x),
            
            movieTitle.topAnchor.constraint(equalTo: movieImage.bottomAnchor, constant: .padding2x),
            movieTitle.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            movieTitle.trailingAnchor.constraint(lessThanOrEqualTo: containerView.trailingAnchor, constant: -.padding2x),
            
            blueSeparatorView.topAnchor.constraint(equalTo: movieTitle.bottomAnchor, constant: 12.0),
            blueSeparatorView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            blueSeparatorView.heightAnchor.constraint(equalToConstant: 12.0),
            blueSeparatorView.widthAnchor.constraint(equalToConstant: 181.0),
            
            stackView.topAnchor.constraint(equalTo: blueSeparatorView.bottomAnchor, constant: .padding2x),
            stackView.trailingAnchor.constraint(lessThanOrEqualTo: containerView.trailingAnchor, constant: -.padding2x),
            stackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            
            descriptionLabel.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 24),
            descriptionLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            descriptionLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.padding2x),
            
            categoryStackView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: .padding2x),
            categoryStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            categoryStackView.trailingAnchor.constraint(lessThanOrEqualTo: containerView.trailingAnchor, constant: -.padding2x),

            
            graySeparatorView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            graySeparatorView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.padding2x),
            graySeparatorView.topAnchor.constraint(equalTo: categoryStackView.bottomAnchor, constant: .padding3x),
            graySeparatorView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -.extraLargePadding)
            
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    
    private func initMovieParameters() {
        movieImage.image = UIImage(named: movie.imageName)
        movieTitle.text = movie.title
        descriptionLabel.text = movie.description
    }
}
