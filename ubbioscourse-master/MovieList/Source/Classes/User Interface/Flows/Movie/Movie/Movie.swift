//
//  Movie.swift
//  MovieList
//
//  Created by babycougar on 03.05.2021.
//

import Foundation

struct Movie {
    let thumbnailImageName: String
    let imageName: String
    let title: String
    let year: String
    let ageGroup: String
    let duration: String
    let language: String
    let description: String
    let categories: [String]
    let ratingValue: String
}

extension Movie {
    static let availableMovies = [
        Movie(thumbnailImageName: "hobbit1icon",
              imageName: "hobbit1",
              title: "The Hobbit: An Unexpected Journey",
              year: "2012", ageGroup: "PG-13",
              duration: "2h 49min",
              language: "Eng",
              description: "A reluctant Hobbit, Bilbo Baggins,sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home," +
                " and the gold within it from the dragon Smaug.",
              categories: ["Adventure", "Fantasy", "Action", "Popular"],
              ratingValue: "7.8"),
    
        Movie(thumbnailImageName: "hobbit2icon",
              imageName: "hobbit2",
              title: "The Hobbit: The Desolation of Smaug",
              year: "2013",
              ageGroup: "PG-13",
              duration: "2h 41min",
              language: "Eng",
              description: "The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug." +
                " Bilbo Baggins is in possession of a mysterious and magical ring.",
              categories: ["Adventure", "Fantasy", "Action", "Popular"],
              ratingValue: "7.8"),
        
        Movie(thumbnailImageName: "hobbit3icon",
              imageName: "hobbit3",
              title: "The Hobbit: The Battle of the Five Armies",
              year: "2014",
              ageGroup: "PG-13",
              duration: "2h 24min",
              language: "Eng",
              description: "Bilbo and company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness.",
              categories: ["Adventure", "Fantasy", "Action", "Popular"],
              ratingValue: "7.4"),
        
        Movie(thumbnailImageName: "lord1icon",
              imageName: "lord1",
              title: "The Lord of the Rings: The Fellowship of the Ring",
              year: "2001",
              ageGroup: "PG-14",
              duration: "2h 58min",
              language: "Eng", description: "A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.",
              categories: ["Adventure", "Fantasy", "Action", "Popular"],
              ratingValue: "7.9"),
        
        Movie(thumbnailImageName: "lord2icon",
              imageName: "lord2",
              title: "The Lord of the Rings: The Two Towers",
              year: "2002",
              ageGroup: "PG-14",
              duration: "2h 59min",
              language: "Eng",
              description: "While Frodo and Sam edge closer to Mordor with the help of the shifty Gollum," +
                " the divided fellowship makes a stand against Sauron's new ally, Saruman, and his hordes of Isengard.",
              categories: ["Adventure", "Fantasy", "Action", "Popular"],
              ratingValue: "8.5"),
        
        Movie(thumbnailImageName:  "lord3icon",
              imageName: "lord3",
              title: "The Lord of the Rings: The Return of the King",
              year: "2003",
              ageGroup: "PG-14",
              duration: "3h 21min",
              language: "Eng",
              description: "Gandalf and Aragorn lead the World of Men against Sauron's army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.",
              categories: ["Adventure", "Fantasy", "Action", "Popular"],
              ratingValue: "8.9")
    ]
}
