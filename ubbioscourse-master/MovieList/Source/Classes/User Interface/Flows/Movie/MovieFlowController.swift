//
//  MovieFlowController.swift
//  MovieList
//
//  Created by babycougar on 21.04.2021.
//

import UIKit

class MovieFlowController: NavigationFlowController {
    
    override var firstScreen: UIViewController {
        let movieListViewController = MovieListViewController()
        movieListViewController.flowDelegate = self
        return movieListViewController
    }
}

extension MovieFlowController: MovieListFlowDelegate {
    func didSelectMovie(movie: Movie) {
        navigationController.pushViewController(MovieDetailViewController(movie: movie), animated: true)
    }
}
