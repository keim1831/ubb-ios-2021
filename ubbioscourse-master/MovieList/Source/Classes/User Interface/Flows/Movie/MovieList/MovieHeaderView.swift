//
//  MovieHeaderView.swift
//  MovieList
//
//  Created by babycougar on 04.05.2021.
//

import UIKit

final class MovieHeaderView: UITableViewHeaderFooterView {
    
    // MARK: - Private properties
        
    private var titleLabel: UILabel!
    private var blueView: UIView!
    
    // MARK: - Init
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        initUI()
        initConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    
    private func initUI() {
        contentView.backgroundColor = .lightGray
        titleLabel = UILabel()
        titleLabel.text = "My Movies"
        titleLabel.textColor = .label
        titleLabel.font = UIFont.systemFont(ofSize: 32.0, weight: .bold)
        titleLabel.backgroundColor = .lightGray
        contentView.addSubview(titleLabel)
        
        blueView = UIView()
        blueView.backgroundColor = .lighterBlue
        contentView.addSubview(blueView)
    }
    
    private func initConstraints() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        blueView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .smallPadding),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding2x),
            titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor),
            blueView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .oneAndHalfPadding),
            blueView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            blueView.widthAnchor.constraint(equalToConstant: 181.0),
            blueView.heightAnchor.constraint(equalToConstant: 12.0)
        ])
    }
}
