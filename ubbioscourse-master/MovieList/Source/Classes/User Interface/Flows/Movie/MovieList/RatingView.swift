//
//  RatingView.swift
//  MovieList
//
//  Created by babycougar on 04.05.2021.
//

import UIKit

final class RatingView: UIView {
    
    // MARK: - Public properties
    
    var ratingValue: String? {
        didSet {
            ratingLabel.text = ratingValue
        }
    }
    
    // MARK: - Private properties
    
    private var ratingLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12.0, weight: .bold)
        label.textColor = .white
        return label
    }()
    
    // MARK: - UI
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initUI()
        initLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initUI() {
        backgroundColor = .lighterBlue
        layer.cornerRadius = 4.0
        layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        addSubview(ratingLabel)
    }
    
    private func initLayout () {
        ratingLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            ratingLabel.topAnchor.constraint(equalTo: topAnchor, constant: .smallPadding),
            ratingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            ratingLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            ratingLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

}
