//
//  MovieListViewController.swift
//  MovieList
//
//  Created by babycougar on 21.04.2021.

import UIKit

protocol MovieListFlowDelegate: AnyObject {
    func didSelectMovie(movie: Movie)
}

final class MovieListViewController: UIViewController {
        
    private var tableView = UITableView()
    
    weak var flowDelegate: MovieListFlowDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        initConstraints()
    }
    
    private func initUI() {
        view.backgroundColor = .lightGray
        tableView.backgroundColor = .lightGray
        tableView.dataSource = self
        tableView.register(MovieCell.self, forCellReuseIdentifier: Identifiers.movieIdentifier.rawValue)
        tableView.register(MovieHeaderView.self, forHeaderFooterViewReuseIdentifier: Identifiers.movieHeaderIdentifier.rawValue)
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        view.addSubview(tableView)
    }
    
    private func initConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

extension MovieListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Movie.availableMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.movieIdentifier.rawValue, for: indexPath) as? MovieCell else {
            fatalError("Failed too dequeue a cell with identifier: movieIdentifier")
        }
        cell.movie = Movie.availableMovies[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: Identifiers.movieHeaderIdentifier.rawValue)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 79
    }
}

extension MovieListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        flowDelegate?.didSelectMovie(movie: Movie.availableMovies[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
} 
