//
//  MovieCell.swift
//  MovieList
//
//  Created by babycougar on 03.05.2021.
//

import UIKit

class MovieCell: UITableViewCell {
    
    // MARK: - Public properties
    
    var movie: Movie? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Private properties
    
    private var thumbnailImageView: UIImageView!
    private var contentStackView: UIStackView!
    private var titleLabel: UILabel!
    private var yearLabel: UILabel!
    private var languageLabel: UILabel!
    private var movieLanguageLabel: UILabel!
    private var languageStackView: UIStackView!
    private var categoryStackView: UIStackView!
    private var containerView: UIView!
    private var favoriteButton: UIButton!
    private var categoriesName: [String] = []
    private var category1: MovieCategoryView!
    private var category2: MovieCategoryView!
    private var category3: MovieCategoryView!
    private var ratingView: RatingView!

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initUI()
        initConstraints()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - UI
    
    private func initUI() {
        
        containerView = UIView()
        containerView.layer.cornerRadius = .bigggestCornerRadius
        containerView.layer.masksToBounds = true
        containerView.backgroundColor = .white
        containerView.layer.shadowColor = .init(red: 49.0, green: 34.0, blue: 144.0, alpha: 0.05)
        containerView.layer.shadowOffset = .init(width: 2.0, height: 3.0)
        contentView.backgroundColor = .lightGray

        thumbnailImageView = UIImageView()
        thumbnailImageView.layer.cornerRadius = .bigggestCornerRadius
        thumbnailImageView.layer.masksToBounds = true
        containerView.addSubview(thumbnailImageView)
        
        contentStackView = UIStackView()
        contentStackView.axis = .vertical
        containerView.addSubview(contentStackView)
        
        titleLabel = UILabel()
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 3
        titleLabel.font = UIFont.systemFont(ofSize: 18.0, weight: .semibold)
        titleLabel.textAlignment = .left
        contentStackView.addArrangedSubview(titleLabel)
        
        yearLabel = UILabel()
        yearLabel.textColor = .gray
        yearLabel.font = UIFont.systemFont(ofSize: 12.0, weight: .semibold)
        yearLabel.textAlignment = .left
        contentStackView.addArrangedSubview(yearLabel)
        
        languageStackView = UIStackView()
        languageStackView.axis = .horizontal
        contentStackView.addArrangedSubview(languageStackView)
        
        languageLabel = UILabel()
        languageLabel.text = "Language"
        languageLabel.textColor = .black
        languageLabel.font = UIFont.systemFont(ofSize: 12.0)
        languageStackView.addSubview(languageLabel)
        
        movieLanguageLabel = UILabel()
        movieLanguageLabel.textColor = .black
        movieLanguageLabel.font = UIFont.systemFont(ofSize: 12.0, weight: .semibold)
        languageStackView.addSubview(movieLanguageLabel)
    
        categoryStackView = UIStackView()
        categoryStackView.axis = .horizontal
        categoryStackView.distribution = .equalSpacing
        categoryStackView.spacing = .halfPadding
        
        category1 = MovieCategoryView()
        category2 = MovieCategoryView()
        category3 = MovieCategoryView()
        
        /*
        for catName in categoriesName{
            var category = MovieCategoryView()
            category.name = catName
            categoryStackView.addArrangedSubview(category)
        }
        */
        categoryStackView.addArrangedSubview(category1)
        categoryStackView.addArrangedSubview(category2)
        categoryStackView.addArrangedSubview(category3)

        contentStackView.addArrangedSubview(categoryStackView)
        
        favoriteButton = UIButton()
        let emptyStarImg = UIImage(systemName: "star")
        let fullStarImg = UIImage(systemName: "star.fill")
        favoriteButton.setImage(emptyStarImg, for: .normal)
        favoriteButton.setImage(fullStarImg, for: .selected)
        favoriteButton.imageView?.contentMode = .scaleAspectFill
        favoriteButton.addTarget(self, action: #selector(handleFavorite), for: .touchUpInside)
        containerView.addSubview(favoriteButton)
        
        ratingView = RatingView()
        containerView.addSubview(ratingView)
    
        contentView.addSubview(containerView)
    }
    
    private func initConstraints() {        
        thumbnailImageView.translatesAutoresizingMaskIntoConstraints = false
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        yearLabel.translatesAutoresizingMaskIntoConstraints = false
        languageStackView.translatesAutoresizingMaskIntoConstraints = false
        languageLabel.translatesAutoresizingMaskIntoConstraints = false
        movieLanguageLabel.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        favoriteButton.translatesAutoresizingMaskIntoConstraints = false
        ratingView.translatesAutoresizingMaskIntoConstraints = false
        categoryStackView.translatesAutoresizingMaskIntoConstraints = false
        category1.translatesAutoresizingMaskIntoConstraints = false
        category2.translatesAutoresizingMaskIntoConstraints = false
        category3.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .padding),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.padding),
            containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding2x),
            containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.padding2x),
            
            thumbnailImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .oneAndHalfPadding),
            thumbnailImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: .oneAndHalfPadding),
            thumbnailImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -.oneAndHalfPadding),
            thumbnailImageView.widthAnchor.constraint(equalToConstant: .thumbnailWidth),
            thumbnailImageView.heightAnchor.constraint(equalToConstant: .thumbnailHeight),
            
            contentStackView.topAnchor.constraint(equalTo: containerView.topAnchor),
            contentStackView.leadingAnchor.constraint(equalTo: thumbnailImageView.trailingAnchor, constant: .oneAndHalfPadding),
            contentStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: thumbnailImageView.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: contentStackView.leadingAnchor),
            titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: containerView.trailingAnchor, constant: -.padding2x),
            
            yearLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .smallPadding),
            languageLabel.topAnchor.constraint(equalTo: yearLabel.bottomAnchor, constant: .oneAndHalfPadding),

            movieLanguageLabel.leadingAnchor.constraint(equalTo: languageLabel.trailingAnchor, constant: .padding),
            movieLanguageLabel.topAnchor.constraint(equalTo: yearLabel.bottomAnchor, constant: .oneAndHalfPadding),
            
            categoryStackView.topAnchor.constraint(greaterThanOrEqualTo: languageLabel.bottomAnchor, constant: .padding),
            categoryStackView.leadingAnchor.constraint(equalTo: contentStackView.leadingAnchor),
            categoryStackView.trailingAnchor.constraint(equalTo: contentStackView.trailingAnchor, constant: -18.0),
            categoryStackView.bottomAnchor.constraint(equalTo: thumbnailImageView.bottomAnchor),
            
            
            favoriteButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: .favButtonPadding),
            favoriteButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.favButtonPadding),
            favoriteButton.widthAnchor.constraint(equalToConstant: .favbuttonWidth),
            favoriteButton.heightAnchor.constraint(equalToConstant: .favButtonHeight),
            
            ratingView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20.0),
            ratingView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            ratingView.widthAnchor.constraint(equalToConstant: 42.0),
            ratingView.heightAnchor.constraint(equalToConstant: 20.0)

        ])
    }

    private func updateUI() {
        guard let movie = movie else { return }
        thumbnailImageView.image = UIImage(named: movie.thumbnailImageName)
        titleLabel.text = movie.title
        yearLabel.text = movie.year
        movieLanguageLabel.text = movie.language
        ratingView.ratingValue = movie.ratingValue
        
        category1.name = movie.categories[0]
        category2.name = movie.categories[1]
        category3.name = movie.categories[2]
        
        /*
        for catName in movie.categories {
            categoriesName.append(catName)
        }
        */
    }
    
    @objc private func handleFavorite() {
        favoriteButton.isSelected = !favoriteButton.isSelected
    }
}
